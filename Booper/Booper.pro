TEMPLATE = app

QT += qml quick
CONFIG += c++11

SOURCES += main.cpp

SOURCES += boopalarm.cpp


ios {
SOURCES += boopalarm.mm
}
android {
SOURCES += boopalarm.android.cpp
QT += androidextras
}
!ios:!android {
    SOURCES += boopalarm.desktop.cpp
}

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    android/src/io/qt/DateStamper/NotificationClient.java \
    android/src/io/qt/DateStamper/NotificationPublisher.java

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

HEADERS += \
    boopalarm.h

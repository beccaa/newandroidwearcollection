#ifndef BOOPALARM_H
#define BOOPALARM_H

#include <QObject>

class BoopAlarm : public QObject
{
    Q_OBJECT
public:
    explicit BoopAlarm(QObject *parent = nullptr);

    QString getSmartMessage(int number);
signals:

public slots:
    void testThisShit();
    void setAlarmPlease(int hours, int minutes = 0);
};

#endif // BOOPALARM_H

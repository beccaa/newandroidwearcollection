#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "boopalarm.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    engine.rootContext()->setContextProperty("scheduler", new BoopAlarm);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}

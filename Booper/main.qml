import QtQuick 2.5
import QtQuick.Window 2.2
import Qt.labs.settings 1.0
import QtQml 2.0

Window {
    visible: true
    width: 320
    height: 320
    id: root
    property int margin: 30

    property int hoursToGo: 3

    Rectangle {
        id: topper
        height: parent.height/2
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        color: "white"
        Rectangle {
            height: parent.height - margin
            width: parent.width - margin
            color: "red"
            anchors.centerIn: parent

            Rectangle {
                anchors.fill: parent
                opacity: 0
                color: "black"
                MouseArea {
                    anchors.fill: parent
                    onDoubleClicked: {
                        parent.opacity = 0.9
                        var deit = new Date()
                        var strng = "";
                        var miuntes = deit.getMinutes();
                        var minz = (miuntes<10 ? "0" : "") + miuntes;

                        strng += deit.getHours() + ":" + minz + ", " + (getMonthed(deit.getMonth())) + " " + deit.getDate() + " " +
                                deit.getFullYear()
                        strng += "\n"
                        deit.setHours(deit.getHours() + hoursToGo)
                        settings.dateMe = deit
                        scheduler.setAlarmPlease(hoursToGo)
                        strng += deit.getHours() + ":" + minz + ", " + (getMonthed(deit.getMonth())) + " " + deit.getDate() + " " +
                                deit.getFullYear()
                        textOutput.text = strng
                    }
                }
                Behavior on opacity {
                    SequentialAnimation {
                        PropertyAnimation {
                            duration: 50
                        }
                        PropertyAnimation {
                            to: 0
                            duration: 850
                        }
                    }
                }
            }

        }
    }
    Rectangle {
        id: hopper
        anchors.top: topper.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        Behavior on color {
            PropertyAnimation{ duration: 200 }
        }

        Text {
            id: textOutput
            text: "No Data"
            anchors.centerIn: parent
        }
        Timer {
            id: fuckTime
            onTriggered: hopper.color = "white"
            interval: 5000
            repeat: false
            running: false
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                hopper.color = "yellow"
                scheduler.testThisShit()
                fuckTime.start()
            }
        }
    }

    property var qtsucks;
    Settings {
        id: settings
        property alias stamp: textOutput.text
        property alias dateMe: root.qtsucks
    }

    Component.onCompleted: {
        if (settings.dateMe !== undefined) {
            var now = new Date()
            var fuck = settings.dateMe - now
            fuck /= 1000
            fuck /= 60
            Math.round(fuck)
            if (fuck > 0) {
                // seconds to the alarum
                scheduler.setAlarmPlease(0, fuck)
            }

            console.log(settings.dateMe, now,  fuck)
        }
        else {
            console.log("This is bullshit")
            console.log(settings.dateMe)
        }
    }

    function getMonthed(month) {
        switch (month) {
        case 0:
            return "Jan"
        case 1:
            return "Feb"
        case 2:
            return "Mar"
        case 3:
            return "April"
        case 4:
            return "May"
        case 5:
            return "June"
        case 6:
            return "July"
        case 7:
            return "Aug"
        case 8:
            return "Sept"
        case 9:
            return "Oct"
        case 10:
            return "Nor"
        case 11:
            return "Dec"
        }
    }
}

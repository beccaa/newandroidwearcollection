import QtQuick 2.0

/* *********************************************
//
// A Seriously Ugly Calculator for Android Wear.
//
// Made in California.
//
********************************************* */

Rectangle {

    color: "black"
    property color numberButtonColor: "#c0c0c0"
    property color operatorButtonColor: "#ffa500"

    property string defaultValue: "0"
    property string valueBuffer: defaultValue
    property bool hasDecimalPoint: false


    property real entryValue: 0.0

    // 0 = no pending operation
    // 1 = divide
    // 2 = multiply
    // 3 = add
    // 4 = subtract
    property int pendingOperation: 0
    property bool calculonInProgress: false
    property real calculonBuffer: 0.0

    onValueBufferChanged:  {
        outputter.text = valueBuffer
        squidgeTextIntoBox()
    }

    function squidgeTextIntoBox() {
        if (outputter.contentWidth < outputter.width) {
            outputter.font.pixelSize = l1.height - 4
        }

        while (outputter.contentWidth > outputter.width) {
            outputter.font.pixelSize--
        }
    }

    function addNumberToBuffer(number) {

        if (calculonInProgress) {
            valueBuffer = defaultValue
            calculonInProgress = false
        }

        var newBuffer = ""

        if (valueBuffer.length === 0) {
            // This should be illegal
            newBuffer = "All Your Circuits <3"
        }
        else if (valueBuffer.length === 1) {
            if (valueBuffer[0] === "0") {
                if (number === ".") {
                    if (hasDecimalPoint) {
                        newBuffer = valueBuffer
                    }
                    else {
                        hasDecimalPoint = true
                        newBuffer = valueBuffer + number
                    }
                }
                else if (number === "0") {
                    newBuffer = valueBuffer
                }
                else {
                    newBuffer = number
                }
            }
            else {
                if (number === ".") {
                    if (hasDecimalPoint) {
                        newBuffer = valueBuffer
                    }
                    else {
                        hasDecimalPoint = true
                        newBuffer = valueBuffer + number
                    }
                }
                else {
                    newBuffer = valueBuffer + number
                }
            }
        }
        else if (valueBuffer.length > 1) {
            if (number === ".") {
                if (hasDecimalPoint) {
                    newBuffer = valueBuffer
                }
                else {
                    hasDecimalPoint = true
                    newBuffer = valueBuffer + number
                }
            }
            else
                newBuffer = valueBuffer + number
        }
        valueBuffer = newBuffer
    }

    /*
      For a new operation: copy the existing buffer into a total buffer,
        store what the pendingOperation is.
      */


    function doOperation(operation) {
        if (operation === 5) {
            if (pendingOperation === 0) {
                // do nothing
            }
            else {
                entryValue = valueBuffer
                switch (pendingOperation) {
                case 1:
                    calculonBuffer = calculonBuffer / entryValue
                    break;
                case 2:
                    calculonBuffer = calculonBuffer * entryValue
                    break;
                case 3:
                    calculonBuffer = calculonBuffer + entryValue
                    break;
                case 4:
                    calculonBuffer = calculonBuffer - entryValue
                    break;
                }
                valueBuffer = calculonBuffer
                pendingOperation = 0
                calculonInProgress = true
                runningTotalText.text = ""
            }
            calculonBuffer = ""
        }
        else if (pendingOperation === 0) {
            pendingOperation = operation
            calculonBuffer = valueBuffer
            valueBuffer = defaultValue
        }
        else {
            entryValue = valueBuffer

            switch (pendingOperation) {
            case 1:
                calculonBuffer = calculonBuffer / entryValue
                break;
            case 2:
                calculonBuffer = calculonBuffer * entryValue
                break;
            case 3:
                calculonBuffer = calculonBuffer + entryValue
                break;
            case 4:
                calculonBuffer = calculonBuffer - entryValue
                break;
            }
            valueBuffer = calculonBuffer
            calculonInProgress = true
            pendingOperation = operation
        }

    }

    function operatorDivide() {
        doOperation(1)
    }

    function operatorMultiply() {
        doOperation(2)
    }

    function operatorPlus() {
        doOperation(3)
    }

    function operatorMinus() {
        doOperation(4)
    }

    function operatorEquals() {
        doOperation(5)
    }

    function operatorAllClear() {
        valueBuffer = defaultValue
        hasDecimalPoint = false
        entryValue = 0.0
        calculonInProgress = false
        pendingOperation = 0
    }

    function operatorPlusMinus() {
        if (valueBuffer[0] === "-") {
            valueBuffer = valueBuffer.substring(1)
        }
        else {
            valueBuffer = "-" + valueBuffer
        }
    }

    function operatorPerCent() {
        entryValue = valueBuffer
        entryValue /= 100
        valueBuffer = entryValue
    }

    onCalculonBufferChanged: {
        if (calculonBuffer !== 0)
            runningTotalText.text = calculonBuffer
    }

    onPendingOperationChanged: {
        var opcode = ""
        switch (pendingOperation) {
        case 1:
            opcode = "÷"
            break;
        case 2:
            opcode = "x"
            break;
        case 3:
            opcode = "+"
            break;
        case 4:
            opcode = "-"
            break;
        }
        operatorText.text = opcode

    }

    Rectangle {
        id: l1

        color: "transparent"
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        height: parent.height/4

        Text {
            id: runningTotalText
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.right: outputter.left
            height: parent.height / 2
            font.pixelSize: height
            color: "white"
        }
        Text {
            id: operatorText
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.right: outputter.left
            height: parent.height / 2
            font.pixelSize: height
            color: "white"
        }

        Text {
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: parent.width * 0.85
            horizontalAlignment: Text.AlignRight
            id: outputter
            font.pixelSize: parent.height - 2
            color: "white"
        }
    }
    Row {
        id: l2

        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: l1.bottom
        height: (parent.height-l1.height)/5

        CalculonButton {
            height: parent.height
            width: parent.width/4
            color: numberButtonColor
            text: "AC"
            onClicked: {
                operatorAllClear()
            }
        }
        CalculonButton {
            height: parent.height
            width: parent.width/4
            color: numberButtonColor
            text: "±"
            onClicked: {
                operatorPlusMinus()
            }
        }
        CalculonButton {
            height: parent.height
            width: parent.width/4
            color: numberButtonColor
            text: "%"
            onClicked: {
                operatorPerCent()
            }
        }
        CalculonButton {
            height: parent.height
            width: parent.width/4
            color: operatorButtonColor
            text: "÷"
            onClicked: {
                operatorDivide()
            }
        }
    }

    Row {
        id: l3

        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: l2.bottom
        height: (parent.height-l1.height)/5

        CalculonButton {
            height: parent.height
            width: parent.width/4
            color: numberButtonColor
            text: "7"
            onClicked: {
                addNumberToBuffer(text)
            }
        }
        CalculonButton {
            height: parent.height
            width: parent.width/4
            color: numberButtonColor
            text: "8"
            onClicked: {
                addNumberToBuffer(text)
            }
        }
        CalculonButton {
            height: parent.height
            width: parent.width/4
            color: numberButtonColor
            text: "9"
            onClicked: {
                addNumberToBuffer(text)
            }
        }
        CalculonButton {
            height: parent.height
            width: parent.width/4
            color: operatorButtonColor
            text: "x"
            onClicked: {
                operatorMultiply()
            }
        }
    }

    Row {
        id: l4

        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: l3.bottom
        height: (parent.height-l1.height)/5
        CalculonButton {
            height: parent.height
            width: parent.width/4
            color: numberButtonColor
            text: "4"
            onClicked: {
                addNumberToBuffer(text)
            }
        }
        CalculonButton {
            height: parent.height
            width: parent.width/4
            color: numberButtonColor
            text: "5"
            onClicked: {
                addNumberToBuffer(text)
            }
        }
        CalculonButton {
            height: parent.height
            width: parent.width/4
            color: numberButtonColor
            text: "6"
            onClicked: {
                addNumberToBuffer(text)
            }
        }
        CalculonButton {
            height: parent.height
            width: parent.width/4
            color: operatorButtonColor
            text: "-"
            onClicked: {
                operatorMinus()
            }
        }
    }

    Row {
        id: l5

        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: l4.bottom
        height: (parent.height-l1.height)/5
        CalculonButton {
            height: parent.height
            width: parent.width/4
            color: numberButtonColor
            text: "1"
            onClicked: {
                addNumberToBuffer(text)
            }
        }
        CalculonButton {
            height: parent.height
            width: parent.width/4
            color: numberButtonColor
            text: "2"
            onClicked: {
                addNumberToBuffer(text)
            }
        }
        CalculonButton {
            height: parent.height
            width: parent.width/4
            color: numberButtonColor
            text: "3"
            onClicked: {
                addNumberToBuffer(text)
            }
        }
        CalculonButton {
            height: parent.height
            width: parent.width/4
            color: operatorButtonColor
            text: "+"
            onClicked: {
                operatorPlus()
            }
        }
    }

    Row {
        id: l6

        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: l5.bottom
        height: (parent.height-l1.height)/5
        CalculonButton {
            height: parent.height
            width: parent.width/2
            color: numberButtonColor
            text: "0"
            onClicked: {
                addNumberToBuffer(text)
            }
        }
        CalculonButton {
            height: parent.height
            width: parent.width/4
            color: numberButtonColor
            text: "."
            onClicked: {
                        addNumberToBuffer(text)
            }
        }
        CalculonButton {
            height: parent.height
            width: parent.width/4
            color: operatorButtonColor
            text: "="
            onClicked: {
                operatorEquals()
            }
        }
    }
}

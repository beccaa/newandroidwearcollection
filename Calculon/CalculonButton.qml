import QtQuick 2.0

Rectangle {

    id: root
    radius: width/4
    property alias text: txt.text
    signal clicked();

    border.width: 2
    border.color: "black"

    Text {
        id: txt
        anchors.centerIn: parent
        font.pixelSize: parent.height - 10
    }

    Rectangle {
        id: overlay
        anchors.fill: parent
        radius: parent.radius
        color: "black"
        opacity: 0

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if (deic.running)
                    deic.stop()
                deic.start()
                root.clicked()
            }
        }
    }
    SequentialAnimation {
        id: deic
        PropertyAnimation {
            target: overlay
            property: "opacity"
            to: 1
            duration: 50
        }
        PropertyAnimation {
            target: overlay
            property: "opacity"
            from: 1; to: 0
            duration: 800
        }
    }
}

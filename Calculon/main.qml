import QtQuick 2.7
import QtQuick.Window 2.2

Window {
    visible: true
    width: 320
    height: 320
    title: qsTr("CALCULON")


    Calculon {
        anchors.fill: parent
    }
}

TEMPLATE = app

TARGET = ChatWatch

QT += qml quick websockets
CONFIG += c++11

SOURCES += main.cpp \
    eliza.cpp \
    elizarelay.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    eliza.h \
    elizarelay.h

DISTFILES += \
    MultiTapButton.qml \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    eliza.dat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

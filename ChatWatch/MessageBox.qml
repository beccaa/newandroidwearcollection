import QtQuick 2.0

Rectangle {
    property alias text: innerText.text
    color: "transparent"
    width: parent.width * 0.9
    height: parent.height * 0.9
    opacity: 0
    onOpacityChanged: {
        if (innerTextTimer.running)
            innerTextTimer.stop()
        innerTextTimer.start()
    }
    FontLoader {
        id: titillium
        source: "titilliumweb/TitilliumWeb-Bold.ttf"
    }
    Behavior on opacity {
        PropertyAnimation {
            duration: 150
        }
    }
    Image {
        source: "messageboxbg.png"
        anchors.fill: parent

        Text {
            id: innerText
            anchors.centerIn: parent
            color: "white"
            wrapMode: Text.WordWrap
            width: parent.width * 0.9
//            height: parent.height;// * 0.8
            font.pixelSize: 25
            font.bold: true
            font.family: titillium.name
        }
    }

    Timer {
        id: innerTextTimer
        interval: 2000
        onTriggered: {
            parent.opacity = 0
        }
    }
}

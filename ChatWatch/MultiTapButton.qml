import QtQuick 2.0

Rectangle {

    width: 75
    height: 70

    radius: width / 2
    border.color: "black"
    border.width: 1

    id: root
    color: "white"
    signal clicked()
    property alias text: toxt.text
    property alias letters: tixt.text
    property bool centerAlign: false

    Text {
        id: toxt
//        anchors.top: parent.top
        x: (parent.width - width)/2
    }
    Component.onCompleted: {
        if (centerAlign) {
            toxt.anchors.centerIn = root
        }
        else {
            toxt.activeFocus.top = root.top
        }
    }

    Text {
        id: tixt
        anchors.top: toxt.bottom
        x: (parent.width - width)/2
        font.pixelSize: toxt.font.pixelSize-2
    }

    function clickHandler() {
        clicked()
    }

    Rectangle {
        id: overlay
        color: "black"
        opacity: 0
        anchors.fill: parent
        radius: parent.radius
        Behavior on opacity {
            PropertyAnimation {
                duration: 150
            }
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            overlay.opacity = 1
            t.start()
            root.clickHandler()
        }
    }

    Timer {
        id: t
        interval: 80
        onTriggered: {
            overlay.opacity = 0
        }
    }
}

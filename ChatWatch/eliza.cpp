#include "eliza.h"

//#include "elizarelay.h"

#include <QDebug>

#include <QFile>
#include <QMap>

Eliza::Eliza(QObject *parent) : QObject(parent)
{
//    m_relay = ElizaRelay::getInstance();
}

Eliza::~Eliza()
{
    foreach (keyword *keyz, m_keywordMap.values())
    {
       delete keyz;
    }
}

void Eliza::init()
{
    // Read the dat file into the Keyword map
    QFile f(":/eliza.dat");

    // Read whole file
    if (f.open(QIODevice::ReadOnly))
    {
        keyword *a_keyword = 0;
        // Go through one line at a time
        QByteArray line = f.readLine().trimmed();
        // start loop
        while (line.length() != 0)
        {
            // if line == keyword line make new keyword and push currrent
            if (line == "@KWD@")
            {
                if (0 != a_keyword)
                {
                    m_keywordMap.insert(a_keyword->m_keyword, a_keyword);
                }
                a_keyword = new keyword;
                a_keyword->m_keyword = "";
            }
            else
            {
                if (a_keyword)
                {
                    // else add line to keyword
                    if (a_keyword->m_keyword == "")
                    {
                        a_keyword->m_keyword = line;
                    }
                    else
                    {
                        a_keyword->m_responses << line;
                    }
                }
            }
            line = f.readLine().trimmed();
        }
        // end loop
        // push final keyword to map. We are ready to go.
        if (a_keyword)
            m_keywordMap.insert(a_keyword->m_keyword, a_keyword);
        f.close();
    }
    else
    {
        qWarning("File no open :(");
    }

    // Set up the word in word outs

    m_wordInWordOuts["ARE"] = "AM"; // 0
    m_wordInWordOuts["AM"] = "ARE"; //  1
    m_wordInWordOuts["WERE"] = "WAS"; // 2
    m_wordInWordOuts["WAS"] = "WERE"; // 3
    m_wordInWordOuts["YOU"] = "ME"; // 4
    m_wordInWordOuts["I"] = "YOU"; // 5
    m_wordInWordOuts["YOUR"] = "MY"; // 6
    m_wordInWordOuts["MY"] = "YOUR"; // 7
    m_wordInWordOuts["I'VE"] = "YOU'VE"; // 8
    m_wordInWordOuts["YOU'VE"] = "I'VE"; // 9
    m_wordInWordOuts["I'M"] = "YOU'RE"; // 10
    m_wordInWordOuts["YOU'RE"] = "I'M"; // 11
    m_wordInWordOuts["ME"] = "YOU"; // 12
    m_wordInWordOuts["YOU"] = "ME"; // 13

    // Create standard responses

    m_resp_null << "Huh ?" << "Whut ?" << "No comprende, dude" << "U WOT M8?";
    m_resp_signon << "Hi, I'm A smart watch. WHAT DO YOU WANT TO TALK ABOUT ?" << "SO HOW ARE YOU DOING TODAY ?" << "HELLO, WHAT'S UP TODAY ?";
    m_resp_noKeyFound << "Please go on..." << "What does that suggest to you ?" << "I see" << "Say whut?" << "I'm not sure I know what you are talking about " << "What's that supposed to mean ?" << "Can you clarify that a bit ?" << "That's interesting..." << "And ????";
    m_resp_bye << "GOOD BYE, HAVE A NICE DAY..." << "BYE, HOPE TO SEE YOU SOON..."<< "BYE AND KEEP IN TOUCH..." << "GOODBYE";
}

void Eliza::writeMessage(QString message)
{
    QString response = putLineToEliza(message);
    emit responseReceived(response);
}


QString Eliza::putLineToEliza(QString line)
{
    // TODO find how magic works
//    m_relay->logMessageFromRemote(line);
    line = line.toUpper();
    keyword *the_key = 0;
    the_key = findKeyword(line);

    QString response;
    if (!the_key)
    {
        int rnd = qrand() % m_resp_noKeyFound.length();
        response = m_resp_noKeyFound[rnd];
    }
    else
    {
        response = randomResponse(the_key);
    }
//    m_relay->logMessageFromServer(response.toLower());
    return response;//.toLower();
}

keyword *Eliza::findKeyword(QString line)
{
    QStringList list = line.split(" ");
    foreach (QString word, list)
    {
        if (m_keywordMap.keys().contains(word))
        {
            return m_keywordMap[word];
        }
    }
    return 0;
}

QString Eliza::randomResponse(keyword *some_keyword)
{
    int number = qrand() % some_keyword->m_responses.length();
    return some_keyword->m_responses.at(number);
}

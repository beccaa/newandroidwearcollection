#ifndef ELIZA_H
#define ELIZA_H

#include <QMap>
#include <QObject>

struct keyword {
    QString         m_keyword;
    int             m_count;
    QStringList     m_responses;
};

//class ElizaRelay;

class Eliza : public QObject
{
    Q_OBJECT
public:
    explicit Eliza(QObject *parent = 0);
    ~Eliza();

    Q_INVOKABLE QString putLineToEliza(QString line);
    void init();
signals:
    void responseReceived(QString messageReceived);

public slots:
    void writeMessage(QString message);

private:

    QMap<QString, keyword*> m_keywordMap;
    QMap<QString, QString> m_wordInWordOuts;
    QStringList m_resp_null, m_resp_noKeyFound, m_resp_signon, m_resp_bye;
//    ElizaRelay *m_relay;

    QString randomResponse(keyword *some_keyword);
    keyword *findKeyword(QString line);

};

#endif // ELIZA_H

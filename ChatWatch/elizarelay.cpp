#include "elizarelay.h"

#include <QDebug>

#include <QDateTime>
#include <QDir>
#include <QFile>
#include <QMutex>

ElizaRelay *ElizaRelay::m_instance = 0;
QMutex *ElizaRelay::m_mutex = new QMutex;

ElizaRelay::ElizaRelay(QObject *parent) : QObject(parent)
{
    QString fileName = QDir::homePath() + QDir::separator() + "eliza_output_" + QDateTime::currentDateTime().toString("hh:mm:ss_dd-MM-yyyy") + ".txt";
    qDebug() << fileName;
    m_file = new QFile(fileName);
}

ElizaRelay::~ElizaRelay()
{
    m_file->flush();
    m_file->close();
}

ElizaRelay *ElizaRelay::getInstance()
{   
    QMutexLocker locker(m_mutex);
    Q_UNUSED(locker)
    if (!m_instance)
    {
        if (!m_instance)
        {
            m_instance = new ElizaRelay;
        }
    }
    return m_instance;
}

void ElizaRelay::logMessageFromRemote(QString message)
{
    message.prepend(" Remote:  ");
    message.prepend(QDateTime::currentDateTime().toString());
    message.append("\n");
    writeTofile(message);
    emit messageFromRemote(message);
}

void ElizaRelay::logMessageFromServer(QString message)
{
    message.prepend(" Server:  ");
    message.prepend(QDateTime::currentDateTime().toString());
    writeTofile(message);
    emit messageFromServer(message);
}

void ElizaRelay::writeTofile(QString message)
{
    if (m_file->open(QIODevice::WriteOnly|QIODevice::Append))
    {
        message.append("\n");
         QTextStream strm(m_file);
         strm << message;
         m_file->flush();
         m_file->close();
    }
    else
    {
        qDebug() << "file won't open :(";
    }
}

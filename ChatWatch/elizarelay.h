#ifndef ELIZARELAY_H
#define ELIZARELAY_H

#include <QFile>
#include <QObject>

class QMutex;

class ElizaRelay : public QObject
{
    Q_OBJECT
public:
    static ElizaRelay *getInstance();
    ~ElizaRelay();

signals:
    void messageFromRemote(QString message);
    void messageFromServer(QString message);

public slots:
    void logMessageFromRemote(QString message);
    void logMessageFromServer(QString message);

private:
    explicit ElizaRelay(QObject *parent = 0);
    static ElizaRelay *m_instance;

    static QMutex *m_mutex;
    QFile *m_file;

    void writeTofile(QString message);
};

#endif // ELIZARELAY_H

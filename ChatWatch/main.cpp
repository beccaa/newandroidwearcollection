#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "eliza.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    Eliza eliza;
    eliza.init();

    //WebSocketWrapper alice;
    engine.rootContext()->setContextProperty("alice", &eliza);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}

import QtQuick 2.6
import QtQuick.Window 2.2
import QtWebSockets 1.0
import QtQuick.Controls 1.4

Window {
    id: root
    visible: true
    width: 320; height: 320

    property string buffer;
    property string entered;
    property string currentButton;

    function messageReceived(message) {
        messagePanel.opacity = 1
        messagePanel.text = message
    }


    Timer {
        id: timer
        interval: 1000
        onTriggered: {
            join()
        }
    }

    onBufferChanged: {
        texto.text = buffer
    }

    function join() {
        if (entered === "_") entered = " "
        buffer += entered
        entered = ""
        currentButton = ""
    }

    function displayEntry() {
        var tmp = buffer + "<b>" + entered + "</b>"
        texto.text = tmp;
        // TODO add a flasing cursor
    }

    function deleteCharacter() {
        buffer = buffer.substring(0, buffer.length - 1)
    }

    Rectangle {
        id: textTangle
        border.color: "black"
        border.width: 1
        radius: 2
        width: parent.width
        height: texto.font.pointSize + 20 // 10 being a magic number
        Text {
            anchors.fill: parent
//            width: parent.width - 2
//            height: parent.height -2
            id: texto
        }
    }
    Column {
        id: keypad
        anchors.top: textTangle.bottom
        anchors.topMargin: 3
        anchors.left: root.left
        width: (75*3); height: parent.height - textTangle.height

        Row {
            MultiTapButton {
                text: "1"
                letters: ""
                onClicked: pushButton(text)
            }
            MultiTapButton {
                text: "2"
                letters: "abc"
                onClicked: pushButton(text)
            }
            MultiTapButton {
                text: "3"
                letters: "def"
                onClicked: pushButton(text)
            }
        }
        Row {
            MultiTapButton {
                text: "4"
                letters: "ghi"
                onClicked: pushButton(text)
            }
            MultiTapButton {
                text: "5"
                letters: "jkl"
                onClicked: pushButton(text)
            }
            MultiTapButton {
                text: "6"
                letters: "mno"
                onClicked: pushButton(text)
            }
        }
        Row {
            MultiTapButton {
                text: "7"
                letters: "pqrs"
                onClicked: pushButton(text)
            }
            MultiTapButton {
                text: "8"
                letters: "tuv"
                onClicked: pushButton(text)
            }
            MultiTapButton {
                text: "9"
                letters: "wxyz"
                onClicked: pushButton(text)
            }
        }
        Row {
            MultiTapButton {
                text: "*"
                onClicked: pushButton(text)
            }
            MultiTapButton {
                text: "0"
                letters: "space"
                onClicked: pushButton(text)
            }
            MultiTapButton {
                text: "#"
                onClicked: pushButton(text)
            }
        }
    }


    Rectangle {
        color: "transparent"
        anchors.top: textTangle.bottom
        anchors.bottom: parent.bottom
        anchors.left: keypad.right
        anchors.right: parent.right

        MultiTapButton {
            id: del
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.left: parent.left
            text: "<x"
            letters: "delete"
            onClicked: {
                timer.stop()
                join()
                deleteCharacter()
            }
        }
        MultiTapButton {

            text: "Send"
            anchors.top: del.bottom
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            centerAlign: true

            onClicked: {
                join()
                alice.writeMessage(texto.text)
                texto.text = ""
                buffer = ""
                entered = ""
            }
        }


    }

    MessageBox {
        id: messagePanel
        anchors.centerIn: parent
    }

    Component.onCompleted: {
        alice.responseReceived.connect(messageReceived)
    }

    function commit(character) {
        timer.stop()
        join()
    }

    function pushButton(button) {
        if (button === "1") {
            if (timer.running && currentButton === "1") {
                if (entered === "") entered = "1";
                else if (entered === "1") entered = "."
                else if (entered === ".") entered = ","
                else if (entered === ",") entered = ":"
                else if (entered === ":") entered = "("
                else if (entered === "(") entered = ")"
                else if (entered === ")") entered = "!"
                else if (entered === "!") entered = "?"
                else if (entered === "?") entered = "1"
                timer.restart()
            }
            else {
                join()
                currentButton = "1"
                entered = "."
                if (timer.running) timer.stop()
                timer.start()
            }
            displayEntry()
        }
        else if (button === "2") {
            if (timer.running && currentButton === "2") {
                if (entered === "") entered = "2";
                else if (entered === "2") entered = "a"
                else if (entered === "a") entered = "b"
                else if (entered === "b") entered = "c"
                else if (entered === "c") entered = "2"
                timer.restart()
            }
            else {
                join()
                currentButton = "2"
                entered = "a"
                if (timer.running) timer.stop()
                timer.start()
            }
            displayEntry()
        }
        else if (button === "3") {
            if (timer.running && currentButton === "3") {
                if (entered === "") entered = "3";
                else if (entered === "3") entered = "d"
                else if (entered === "d") entered = "e"
                else if (entered === "e") entered = "f"
                else if (entered === "f") entered = "3"
                timer.restart()
            }
            else {
                join()
                currentButton = "3"
                entered = "d"
                if (timer.running) timer.stop()
                timer.start()
            }
            displayEntry()
        }
        else if (button === "4") {
            if (timer.running && currentButton === "4") {
                if (entered === "") entered = "4";
                else if (entered === "4") entered = "g"
                else if (entered === "g") entered = "h"
                else if (entered === "h") entered = "i"
                else if (entered === "i") entered = "4"
                timer.restart()
            }
            else {
                join()
                currentButton = "4"
                entered = "g"
                if (timer.running) timer.stop()
                timer.start()
            }
            displayEntry()
        }
        else if (button === "5") {
            if (timer.running && currentButton === "5") {
                if (entered === "") entered = "5";
                else if (entered === "5") entered = "j"
                else if (entered === "j") entered = "k"
                else if (entered === "k") entered = "l"
                else if (entered === "l") entered = "5"
                timer.restart()
            }
            else {
                join()
                currentButton = "5"
                entered = "j"
                if (timer.running) timer.stop()
                timer.start()
            }
            displayEntry()
        }
        else if (button === "6") {
            if (timer.running && currentButton === "6") {
                if (entered === "") entered = "6";
                else if (entered === "6") entered = "m"
                else if (entered === "m") entered = "n"
                else if (entered === "n") entered = "o"
                else if (entered === "o") entered = "6"
                timer.restart()
            }
            else {
                join()
                currentButton = "6"
                entered = "m"
                if (timer.running) timer.stop()
                timer.start()
            }
            displayEntry()
        }
        else if (button === "7") {
            if (timer.running && currentButton === "7") {
                if (entered === "") entered = "7";
                else if (entered === "7") entered = "p"
                else if (entered === "p") entered = "q"
                else if (entered === "q") entered = "r"
                else if (entered === "r") entered = "s"
                else if (entered === "s") entered = "7"
                timer.restart()
            }
            else {
                join()
                currentButton = "7"
                entered = "p"
                if (timer.running) timer.stop()
                timer.start()
            }
            displayEntry()
        }
        else if (button === "8") {
            if (timer.running && currentButton === "8") {
                if (entered === "") entered = "8";
                else if (entered === "8") entered = "t"
                else if (entered === "t") entered = "u"
                else if (entered === "u") entered = "v"
                else if (entered === "v") entered = "8"
                timer.restart()
            }
            else {
                join()
                currentButton = "8"
                entered = "t"
                if (timer.running) timer.stop()
                timer.start()
            }
            displayEntry()
        }
        else if (button === "9") {
            if (timer.running && currentButton === "9") {
                if (entered === "") entered = "9";
                else if (entered === "9") entered = "w"
                else if (entered === "w") entered = "x"
                else if (entered === "x") entered = "y"
                else if (entered === "y") entered = "z"
                else if (entered === "z") entered = "9"
                timer.restart()
            }
            else {
                join()
                currentButton = "9"
                entered = "w"
                if (timer.running) timer.stop()
                timer.start()
            }
            displayEntry()
        }
        else if (button === "0") {
            if (timer.running && currentButton === "0") {
                if (entered === "") entered = "0";
                else if (entered === "0") entered = "_"
                else if (entered === "_") entered = "0"
                timer.restart()
            }
            else {
                join()
                currentButton = "0"
                entered = "_"
                if (timer.running) timer.stop()
                timer.start()
            }
            displayEntry()
        }
        else if (button === "*") {
            if (timer.running && currentButton === "*") {
                if (entered === "") entered = "*";
                else if (entered === "*") entered = "="
                else if (entered === "+") entered = "*"
                else if (entered === "-") entered = "+"
                else if (entered === "/") entered = "-"
                else if (entered === "=") entered = "/"
                timer.restart()
            }
            else {
                join()
                currentButton = "*"
                entered = "/"
                if (timer.running) timer.stop()
                timer.start()
            }
            displayEntry()
        }
        else if (button === "#") {
            if (timer.running && currentButton === "#") {
                if (entered === "") entered = "#";
                else if (entered === "#") entered = ":)"
                else if (entered === ":)") entered = ":("
                else if (entered === ":(") entered = "Qt"
                else if (entered === "Qt") entered = "#"
                timer.restart()
            }
            else {
                join()
                currentButton = "#"
                entered = "Qt"
                if (timer.running) timer.stop()
                timer.start()
            }
            displayEntry()
        }
    }
}

#ifndef WEBSOCKETWRAPPER_H
#define WEBSOCKETWRAPPER_H

#include <QObject>

class QWebSocket;
class WebSocketWrapper : public QObject
{
    Q_OBJECT
public:
    explicit WebSocketWrapper(QObject *parent = 0);

signals:
    void responseReceived(QString response);

public slots:
    void confgureSocket();
    void writeMessage(QString message);

private:
    QWebSocket      *m_socket;
    QString          m_address;
    void openSocket();
    void setAddress(QString address);

private slots:
    void onTextMessageReceived(QString message);
};

#endif // WEBSOCKETWRAPPER_H

import QtCharts 2.0
import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

Rectangle {
    visible: true
    width: 320
    height: 320
//    title: qsTr("Hello World")
    id: root


    Flickable {
        id: herrFlick
        anchors.fill: parent
        contentHeight: 320 * 2

        onContentYChanged: {
            if (youMoveMe.running) youMoveMe.stop()
            var multiplier = Math.round(contentY / root.height)
            var actualSettingShouldBe = root.height * multiplier
            youMoveMe.from = contentY
            youMoveMe.to = actualSettingShouldBe
            var dur = actualSettingShouldBe - contentY
            if (dur < 0) dur *= -1
            dur *= 1
            youMoveMe.duration = dur
//            console.log(dur, flicking, dragging)

            if (!dragging) youMoveMe.start()
        }
        NumberAnimation {
            target: herrFlick
            property: "contentY"
            duration: 200
            easing.type: Easing.InOutQuad
            id: youMoveMe
        }

        Column {
            Rectangle {
                id: page1
                height: root.width; width: root.width

                Rectangle {
                    id: subColorHolder
                    anchors.fill: parent
                    anchors.margins: 30

                    Rectangle {
                        anchors.left: parent.left
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.right: sliderHolder.left

                        Text {
                            anchors.centerIn: parent
                            text: Math.round(tumbler.currentIndex + 50) + "°F"
                            font.pixelSize: 60
                            onTextChanged: {
                                font.pixelSize = 60
                                while (contentWidth > parent.width) {
                                    font.pixelSize--
                                }
                            }
                        }
                    }
                    Rectangle {
                        id: sliderHolder
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
    //                    anchors.margins: 30
                        width: 120

                        function popColor(starto) {
                            var red = starto
                            red = starto/25
                            red *= 255
                            red = Math.round(red)
                            var blue = 255- red
//                            if (red >= blue) blue = 0
//                            else red = 0

                            page1.color = rgbToHex(red, 0, blue)
                        }

                        function rgb2Hex(red, green, blue) {
                              var rgb = blue | (green << 8) | (red << 16);
                              return '#' + (0x1000000 + rgb).toString(16).slice(1)
                        }

                        function rgbToHex(r, g, b) {
                            return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
                        }

                        Component.onCompleted: popColor(tumbler.currentIndex)

                        Tumbler {
                            id: tumbler
                            opacity: 1
                            anchors.fill: parent
                            model: 25
                            wrap: false
                            delegate: Rectangle {
                                border.color: "darkgrey"
                                border.width: 2
                                color: "#c0c0c0"
                                width: parent.width
                                height: 20
                                opacity: 0.3 + Math.max(0, 1 - Math.abs(Tumbler.displacement)) * 0.7
                                Text {
                                    anchors.centerIn: parent
                                    text: (index + 50)
                                }
                            }
                            onCurrentIndexChanged: {
                                parent.popColor(currentIndex)
                            }
                            Component.onCompleted: currentIndex = 20
                        }
                }


                }

            }

            Rectangle {
                height: root.width; width: root.width

                MiniChartView {
                    anchors.fill: parent
                }

                color: "yellow"
            }

        }

    }


}

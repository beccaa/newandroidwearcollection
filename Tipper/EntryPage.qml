import QtQuick 2.0
import QtQuick.Controls 2.1

Rectangle {
    id: r

    property string buttonColor: "#c0c0c0"
    property string marginColor: "white"
    property int marginWeight: 4
//    property string currency: "$"
    property int currencyIndex: 0
    property var currencies: [ "$", "€", "NOK" ]

    signal calculateTip(var total, var currency, var currencyBefore);

    Component.onCompleted: {
        //currencyText.setText()

        // Debug & dev values
//        textOutput.addDigit("1")
//        textOutput.addDigit("5")
//        textOutput.addDigit("8")
//        textOutput.addDigit("1")
    }

    Component {
        id: tumbleDelegate
        Rectangle {
            border.color: "darkgrey"
            border.width: 2
            color: "#c0c0c0"
//            width: parent.width
//            height: parent.height
            opacity: 0.3 + Math.max(0, 1 - Math.abs(Tumbler.displacement)) * 0.7
            Text {
                anchors.centerIn: parent
                text: index
            }
        }
    }

//    Column {
//        anchors.fill: parent

        Item {
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            height: parent.height/2

            Row {
                height: r.height/2
                Item {
                    height: parent.height
                    width: dollar.contentWidth + 4
                    Text {
                        id: dollar
                        anchors.centerIn: parent
                        text: "$"
                    }
                }
                Tumbler {
                    id: td1
                    model: 10
                    visibleItemCount: 2
                    delegate: tumbleDelegate
                    height: r.height/2
                    width: (r.width - dot.contentWidth - dollar.contentWidth - 8)/5
                }
                Tumbler {
                    id: td2
                    model: 10
                    visibleItemCount: 2
                    delegate: tumbleDelegate
                    currentIndex: 1
                    height: r.height/2
                    width: (r.width - dot.contentWidth - dollar.contentWidth - 8)/5
                }
                Tumbler {
                    id: td3
                    model: 10
                    visibleItemCount: 2
                    delegate: tumbleDelegate
                    currentIndex: 5
                    height: r.height/2
                    width: (r.width - dot.contentWidth - dollar.contentWidth - 8)/5
                }
                Item {
                    height: parent.height
                    width: dot.contentWidth + 4
                    Text {
                        id: dot
                        anchors.centerIn: parent
                        text: "."
                    }
                }
                Tumbler {
                    id: tc1
                    model: 10
                    visibleItemCount: 2
                    delegate: tumbleDelegate
                    currentIndex: 5
                    height: r.height/2
                    width: (r.width - dot.contentWidth - dollar.contentWidth - 8)/5
                }
                Tumbler {
                    id: tc2
                    model: 10
                    visibleItemCount: 2
                    delegate: tumbleDelegate
                    currentIndex: 0
                    height: r.height/2
                    width: (r.width - dot.contentWidth - dollar.contentWidth - 8)/5
                }
            }

        }

        Item {
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: parent.height/2

            Rectangle {
                Rectangle {
                    id: overlay
                    anchors.fill: parent
                    color: "black"
                    opacity: 0
                    function overlayFlash() {
                        flash.start()
                    }
                    SequentialAnimation {
                        id: flash
                        PropertyAnimation {
                            target: overlay
                            property: "opacity"
                            from: 0; to: 0.5
                            duration: 125
                        }
                        PropertyAnimation {
                            target: overlay
                            property: "opacity"
                            from: 0.4; to: 0
                            duration: 4
                        }
                        ScriptAction {
                            script: {
                                var str = "" + td1.currentIndex + td2.currentIndex + td3.currentIndex + tc1.currentIndex + tc2.currentIndex
                                calculateTip(str , currencies[currencyIndex], (currencies[currencyIndex] === "$"));

                            }
                        }
                    }
                }

                height: parent.height
                width: parent.width
                border.color: marginColor
                border.width: marginWeight
                color: buttonColor
                Text {
                    anchors.centerIn: parent
                    text: "Tip Me"
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        overlay.overlayFlash()
                    }
                }
            }
        }


//        }


//        MoneyBox {
//            id: textOutput
//            height: parent.height/3.5
//            width: parent.width
//        }

//        Row {
//            height: (parent.height-textOutput.height-bottomRow.height)/2
//            width: parent.width

//            NumberButton {
//                width: parent.width/5
//                height: parent.height
//                color: buttonColor
//                border.color: marginColor
//                border.width: marginWeight
//                buttonText: "1"
//                onButtonPressed: {
//                    textOutput.addDigit(buttonText)
//                }
//            }
//            NumberButton {
//                width: parent.width/5
//                height: parent.height
//                color: buttonColor
//                border.color: marginColor
//                border.width: marginWeight
//                buttonText: "2"
//                onButtonPressed: {
//                    textOutput.addDigit(buttonText)
//                }
//            }
//            NumberButton {
//                width: parent.width/5
//                height: parent.height
//                color: buttonColor
//                border.color: marginColor
//                border.width: marginWeight
//                buttonText: "3"
//                onButtonPressed: {
//                    textOutput.addDigit(buttonText)
//                }
//            }
//            NumberButton {
//                width: parent.width/5
//                height: parent.height
//                color: buttonColor
//                border.color: marginColor
//                border.width: marginWeight
//                buttonText: "4"
//                onButtonPressed: {
//                    textOutput.addDigit(buttonText)
//                }
//            }
//            NumberButton {
//                width: parent.width/5
//                height: parent.height
//                color: buttonColor
//                border.color: marginColor
//                border.width: marginWeight
//                buttonText: "5"
//                onButtonPressed: {
//                    textOutput.addDigit(buttonText)
//                }
//            }
//        }

//        Row {
//            height: (parent.height-textOutput.height-bottomRow.height)/2
//            width: parent.width

//            NumberButton {
//                width: parent.width/5
//                height: parent.height
//                color: buttonColor
//                border.color: marginColor
//                border.width: marginWeight
//                buttonText: "6"
//                onButtonPressed: {
//                    textOutput.addDigit(buttonText)
//                }
//            }
//            NumberButton {
//                width: parent.width/5
//                height: parent.height
//                color: buttonColor
//                border.color: marginColor
//                border.width: marginWeight
//                buttonText: "7"
//                onButtonPressed: {
//                    textOutput.addDigit(buttonText)
//                }
//            }
//            NumberButton {
//                width: parent.width/5
//                height: parent.height
//                color: buttonColor
//                border.color: marginColor
//                border.width: marginWeight
//                buttonText: "8"
//                onButtonPressed: {
//                    textOutput.addDigit(buttonText)
//                }
//            }
//            NumberButton {
//                width: parent.width/5
//                height: parent.height
//                color: buttonColor
//                border.color: marginColor
//                border.width: marginWeight
//                buttonText: "9"
//                onButtonPressed: {
//                    textOutput.addDigit(buttonText)
//                }
//            }
//            NumberButton {
//                width: parent.width/5
//                height: parent.height
//                color: buttonColor
//                border.color: marginColor
//                border.width: marginWeight
//                buttonText: "0"
//                onButtonPressed: {
//                    textOutput.addDigit(buttonText)
//                }
//            }
//        }

//        Row {
//            id: bottomRow
//            height: parent.height / 7
//            width: parent.width

//            Rectangle {
//                height: parent.height
//                width: parent.width / 2
//                border.color: marginColor
//                border.width: marginWeight
//                color: buttonColor
//                Text {
//                    id: currencyText
//                    anchors.centerIn: parent
//                    text: "CURRENT"
//                    function setText() {
//                        var curStri = ""
//                        var currentcy
//                        for (var i = 0; i < currencies.length; i++) {

//                            if (i !== 0) {
//                                curStri += " / "
//                            }

//                            if (i === currencyIndex) {
//                                curStri += "<b>"
//                                currentcy = currencies[i]
//                            }

//                            curStri += currencies[i];

//                            if (i === currencyIndex) {
//                                curStri += "</b>"
//                            }
//                        }
//                        currencyText.text = curStri
//                        textOutput.setCurrency(currentcy, (currentcy === "$"))
//                    }
//                }
//                MouseArea {
//                    anchors.fill: parent
//                    onClicked: {
//                        if (currencyIndex === (currencies.length-1))
//                            currencyIndex = 0
//                        else
//                            currencyIndex++

//                        currencyText.setText()
//                    }

//                }
//            }
//            Rectangle {
//                height: parent.height
//                width: parent.width / 2
//                border.color: marginColor
//                border.width: marginWeight
//                color: buttonColor
//                Text {
//                    anchors.centerIn: parent
//                    text: "Tip Me"
//                }
//                MouseArea {
//                    anchors.fill: parent
//                    onClicked: {
//                        calculateTip(textOutput.getCurrentValue() , currencies[currencyIndex], (currencies[currencyIndex] === "$"));
//                    }
//                }
//            }
//        }
//    }
}


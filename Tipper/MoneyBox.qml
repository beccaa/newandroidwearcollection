import QtQuick 2.0
import QtQuick.Controls 1.0

Rectangle {

    property alias font: _text.font
    property string currency: "$"
    property bool currencyUnitBefore: true

    function setCurrency(currency, before) {
        this.currency = currency
        currencyUnitBefore = before
        resetBuffer()
    }
    function addDigit(digit) {
        if (!(_text.buffer.length === 0 && digit === "0"))
            _text.buffer += digit;
    }
    function getCurrentValue() {
        return _text.buffer;
    }
    function clearValue() {
        _text.buffer = ""
        _text.font.pixelSize = height
    }

    function backSpace() {
        _text.buffer = _text.buffer.substring(0, (_text.buffer.length-1))
        assessSize()
    }


    Rectangle {
        id: textSpacer
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: deleteButtonHolder.left
        Text {
            id: _text
            anchors.centerIn: parent

            font.pixelSize: parent.height

            property string buffer: ""

            onBufferChanged: {
                resetBuffer()
            }

            onTextChanged: assessSize()
        }
    }

    Rectangle {
        id: deleteButtonHolder
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        width: parent.width/10

        color: "transparent"

        Rectangle {
            width: parent.width
            height: width
            anchors.centerIn: parent
            color: "#c0c0c0"
            radius: 5
            Text {
                anchors.centerIn: parent
                text: "X"
            }
            MouseArea {
                anchors.fill: parent
                onClicked: backSpace()
//                onClicked: clearValue()
            }

        }
    }


    function resetBuffer() {
        var textPreBuffer = ""
        var textToShow = "";

        if (currencyUnitBefore) {
            textToShow += currency
        }
        textPreBuffer = _text.buffer;
        while (textPreBuffer.length < 3) {
            textPreBuffer = "0" + textPreBuffer
        }

        var length = textPreBuffer.length
        for (var j = 0; j < length; j++) {
            if (j === (length-2)) textToShow += "."
            textToShow += textPreBuffer[j]
        }

        if (!currencyUnitBefore) {
            textToShow += " " + currency
        }
        _text.text = textToShow

    }

    function assessSize() {
//        _text.font.pixelSize = textSpacer.height
        if (!(textSpacer.width === 0)) {
            while (_text.contentWidth > textSpacer.width) {
                _text.font.pixelSize--
            }
        }
    }
}

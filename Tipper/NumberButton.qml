import QtQuick 2.0

Rectangle {
    radius: width/6
    property alias buttonText: _text_.text
    signal buttonPressed()
    Text {
        id: _text_
        anchors.centerIn: parent
        text: "Qt"
    }
    Rectangle {
        id: overlay
        anchors.centerIn: parent
        height: parent.height - (2 * parent.border.width)
        width: parent.width- (2 * parent.border.width)
        color: "#444"
        opacity: 0
        radius: parent.radius
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            if (deic.running)
                deic.stop()
            deic.start()
            buttonPressed()
        }
    }

    SequentialAnimation {
        id: deic
        PropertyAnimation {
            target: overlay
            property: "opacity"
            to: 1
            duration: 50
        }
        PropertyAnimation {
            target: overlay
            property: "opacity"
            from: 1; to: 0
            duration: 400
        }
    }
}

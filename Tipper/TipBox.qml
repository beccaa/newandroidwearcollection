import QtQuick 2.0

Rectangle {

    id: root
    property string currency: "$"
    property bool currencyBefore: true
    property int check: 0

    function initValues(check, currency, currencyBefore) {
        view.model = ""
        root.check = check
        root.currency = currency
        root.currencyBefore = currencyBefore
        view.model = model
    }

    signal closeTipPage()

    ListView {
        id: view
        anchors.fill: parent
        delegate: Rectangle {
            height: 100
            width: parent.width
            color: "white"
            radius: 4
            border.color: "black"
            border.width: 1
            Image {
                id: smilimg
                anchors.left: parent.left
                anchors.top: parent.top
                sourceSize.height: parent.height
                sourceSize.width: parent.height
                source: smiley
            }

            Text {
                anchors.left: smilimg.right
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                font.pixelSize: (parent.height-20)/4

                text: "<b>"+name+"</b><br/>Total: " + (currencyBefore?currency:"") + " " + check/100 + (currencyBefore?"": " " + currency) + "<br/>Tip " + multiplier + "%: " + (currencyBefore?currency:"") + ( (multiplier*check)/10000 ) + (currencyBefore?"": " " + currency) + "<br/>Total: " +  (currencyBefore?currency:"") + ((100+multiplier)*check)/10000 + (currencyBefore?"": " " + currency);
            }
            MouseArea {
                anchors.fill: parent
                onClicked: closeTipPage()
            }
        }

    }

    ListModel {
        id: model
        ListElement {
            smiley: "osten.png"
            multiplier: 25
            name: "Ostentatious"
        }
        ListElement {
            smiley: "delighted.png"
            multiplier: 20
            name: "Delighted"
        }
        ListElement {
            smiley: "h2.png"
            multiplier: 15
            name: "Happy"
        }
        ListElement {
            smiley: "neutral.png"
            multiplier: 10
            name: "It's OK"
        }
        ListElement {
            smiley: "sad.png"
            multiplier: 5
            name: "Not good"
        }
        ListElement {
            smiley: "angry.png"
            multiplier: 0
            name: "Terrible"
        }
    }
}

import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtSensors 5.3

Window {
    visible: true
    width: 320
    height: 320


    // TODO refactor the Main page, so that it's basically a big ol' container for
    // multiple pages. Like a stack view. The content below needs to move to it's
    // own UI.

    StackView {
        id: view
        anchors.fill: parent
        initialItem: entry
    }

    property bool showingTips: false

    function noTips() {
        if (showingTips) {
            view.pop()
            showingTips = false
        }
    }

    Item {
        id: entry
        EntryPage {
            anchors.fill: parent
            onCalculateTip: {
                tipBox.initValues(total, currency, currencyBefore)
                view.push(tipJar)
                showingTips = true
            }
        }
    }

    Item {
        id: tipJar
        TipBox {
            id: tipBox
            anchors.fill: parent
            onCloseTipPage: {
                noTips()
            }
        }
    }

    SensorGesture {
        id: elSensor
        enabled: true
        gestures: [ "QtSensors.doubletap", "QtSensors.pickup", "QtSensors.shake2", "QtSensors.slam", "QtSensors.turnover", "QtSensors.shake"]
        onDetected: {
            noTips()
        }
    }



}

import QtQuick 2.5
import QtQuick.Window 2.2
import QtPositioning 5.0
import QtLocation 5.6

Window {
    visible: true
    width: 320
    height: 320

    PositionSource {
        id: positionSource
        onPositionChanged: {
            positionMunchkin()
        }
    }

    Component.onCompleted: {
        spudTimer.start()
    }

    Timer {
        id: spudTimer
        interval: 400
        onTriggered: {
            positionSource.update()
        }
        running: true
        repeat: true
    }

    function positionMunchkin() {
        if (positionSource.position.latitudeValid && positionSource.position.longitudeValid) {
            var address = undefined//positionSource.position.location.address

            if (address === undefined) {
                var lat = positionSource.position.coordinate.latitude * 100
                var longi = positionSource.position.coordinate.longitude * 100
                locationText.text = "Lat: " + ((Math.round(lat)) / 100) + " Long: " + ((Math.round(longi))/100)
            }
        }
        else {
            locationText.text = "We Are Lost"
        }
        if (positionSource.position.speedValid) {
            speedText.text = positionSource.position.speed
        }
        else speedText.text = "--"


    }


    Rectangle {
        id: topRect
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: parent.height / 2
        color: "black"
        Text {
            anchors.centerIn: parent
            id: locationText
            text: "Santa Clara CA"
            color: "white"
        }
    }
    Rectangle {
        id: bottomRect
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        height: parent.height / 2
        color: "black"
        Text {
            anchors.centerIn: parent
            id: speedText
            text: "not moving"
            color: "white"
        }
    }

}

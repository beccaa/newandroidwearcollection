import QtQuick 2.5
import QtQuick.Window 2.2

Window {
    visible: true
    width: 320
    height: 320


    Image {
        id: yay
        anchors.fill: parent
        source: "Yay.jpg"

        Behavior on opacity {
            NumberAnimation {
                duration: 400
                easing.type: Easing.OutBounce
            }
        }
    }


    Timer {
        running: true
        interval:  900
        repeat: true
        onTriggered: {
            if (yay.opacity === 1)
                yay.opacity = 0
            else
                yay.opacity = 1
        }
    }
}
